import numpy as np
from PIL import Image as im
import tensorflow as tf
import glob, os, re, sys
from PSNR import psnr
from scipy.ndimage import imread
import os
import argparse
# from MODEL import model
from MODEL_residual4_layer5 import model

f_size = 64
DATA_PATH = "./data/test/"
path_dir = "./data/test/Set5/"	# Set5, Set14, B100, Urban100
# scale_path_dir = "./Scale/"
result_path_dir = "./Result/"
file_list = os.listdir(path_dir)
file_list.sort()
parser = argparse.ArgumentParser()
parser.add_argument("--model_path")
args = parser.parse_args()
model_path = args.model_path

# www.equasys.de/colorconversion.html
def rgb2ycbcr(img_raw):
	y = 16 + 0.257 * img_raw[:, :, 0] + 0.504 * img_raw[:, :, 1] + 0.098 * img_raw[:, :, 2]
	cb = 128 - 0.148 * img_raw[:, :, 0] - 0.291 * img_raw[:, :, 1] + 0.439 * img_raw[:, :, 2]
	cr = 128 + 0.439 * img_raw[:, :, 0] - 0.368 * img_raw[:, :, 1] - 0.071 * img_raw[:, :, 2]
	img = np.dstack((y,cb,cr))
	return img

def colorize(y, ycbcr):
	y = (y * 255).astype('uint8')
	img = np.dstack((y, ycbcr[:, :, 1], ycbcr[:, :, 2]))
	img = im.fromarray(img, "YCbCr").convert("RGB")
	return img

def test_VDSR_with_sess(epoch, ckpt_path, data_path,sess):
	arr_psnr = []
	for i in range(3):
		arr_psnr.append([])
	saver.restore(sess, ckpt_path)
	for i, item in enumerate(file_list):
		file_name = os.path.splitext(item)
		im_gt_rgb = im.open(path_dir+item).convert("RGB")
		im_raw_y = rgb2ycbcr(np.array(im_gt_rgb))[:,:,0]/255.0
		for x in range(2,5):#About scale X2, X3, X4
			temp = im_gt_rgb.resize((int(im_gt_rgb.size[0]/x), int(im_gt_rgb.size[1]/x)), im.BICUBIC)
			im_gt = temp.resize((int(im_gt_rgb.size[0]), int(im_gt_rgb.size[1])), im.BICUBIC)
			im_gt = np.array(im_gt).astype(np.float32)
			im_gt_ycbcr = rgb2ycbcr(im_gt)
			im_gt_y = im_gt_ycbcr[:,:,0]/255.0
			im_vdsr_y = sess.run([output_tensor], feed_dict={input_tensor: np.resize(im_gt_y, (1, im_gt_y.shape[0], im_gt_y.shape[1], 1))})

			im_vdsr_y = np.resize(im_vdsr_y, (im_gt_y.shape[0], im_gt_y.shape[1]))
			im_vdsr_y = im_vdsr_y.clip(16/255, 235/255)
			image = colorize(im_vdsr_y, im_gt_ycbcr.astype('uint8'))
			image.save(result_path_dir+file_name[0]+"X"+str(x)+"VDSR"+file_name[1])

			arr_psnr[x-2].append(psnr(im_vdsr_y, im_raw_y, x))
	print("X2 PSNR: %f \nX3 PSNR: %f \nX4 PSNR: %f \n" % (np.mean(arr_psnr[0]), np.mean(arr_psnr[1]), np.mean(arr_psnr[2])))
def test_VDSR(epoch, ckpt_path, data_path):
	with tf.Session() as sess:
		test_VDSR_with_sess(epoch, ckpt_path, data_path, sess)
if __name__ == '__main__':
	model_list = sorted(glob.glob("./checkpoints/VDSR_adam_epoch_*"))
	model_list = [fn for fn in model_list if not os.path.basename(fn).endswith("meta")]
	model_list = [fn for fn in model_list if not os.path.basename(fn).endswith("index")]
	if os.path.exists(result_path_dir):
		files = [f for f in os.listdir(result_path_dir)]
		for f in files:
			os.remove(str(result_path_dir+f))
	else:
		os.makedirs(result_path_dir)

	with tf.Session() as sess:
		input_tensor = tf.placeholder(tf.float32, shape=(1, None, None, 1))
		shared_model = tf.make_template('shared_model', model)
		output_tensor, weights 	= shared_model(input_tensor, f_size)
		saver = tf.train.Saver(weights)
		tf.global_variables_initializer().run()
		for model_ckpt in model_list:#read only .data file (started index::step)
			model_ckpt = model_ckpt.split(".data")[0]
			epoch = int(model_ckpt.split('epoch_')[-1].split('(')[0])
			print("Testing ", model_ckpt)
			test_VDSR_with_sess(epoch, model_ckpt, DATA_PATH,sess)