import tensorflow as tf
import numpy as np

def model(input_tensor, f_size):
	with tf.device("/gpu:0"):
		weights = []
		tensor = tensor2 = tensor3 = tensor4 = None

		conv_00_w = tf.get_variable("conv_00_w", [3,3,1,f_size], initializer=tf.random_normal_initializer(stddev=np.sqrt(2.0/9)))
		conv_00_b = tf.get_variable("conv_00_b", [f_size], initializer=tf.constant_initializer(0))
		weights.append(conv_00_w)
		weights.append(conv_00_b)
		tensor = tf.nn.relu(tf.nn.bias_add(tf.nn.conv2d(input_tensor, conv_00_w, strides=[1,1,1,1], padding='SAME'), conv_00_b))

		for i in range(2):
			conv_w = tf.get_variable("conv_%02d_w" % (i+1), [3,3,f_size,f_size], initializer=tf.random_normal_initializer(stddev=np.sqrt(2.0/9/f_size)))
			conv_b = tf.get_variable("conv_%02d_b" % (i+1), [f_size], initializer=tf.constant_initializer(0))
			weights.append(conv_w)
			weights.append(conv_b)
			tensor = tf.nn.relu(tf.nn.bias_add(tf.nn.conv2d(tensor, conv_w, strides=[1,1,1,1], padding='SAME'), conv_b))

		conv_w = tf.get_variable("conv_4_w", [3,3,f_size,1], initializer=tf.random_normal_initializer(stddev=np.sqrt(2.0/9/f_size)))
		conv_b = tf.get_variable("conv_4_b", [1], initializer=tf.constant_initializer(0))
		weights.append(conv_w)
		weights.append(conv_b)
		tensor = tf.nn.bias_add(tf.nn.conv2d(tensor, conv_w, strides=[1,1,1,1], padding='SAME'), conv_b)
		tensor = tf.add(tensor, input_tensor)	#residual 1

		conv_5_w = tf.get_variable("conv_5_w", [3, 3, 1, f_size], initializer=tf.random_normal_initializer(stddev=np.sqrt(2.0 / 9)))
		conv_5_b = tf.get_variable("conv_5_b", [f_size], initializer=tf.constant_initializer(0))
		weights.append(conv_5_w)
		weights.append(conv_5_b)
		tensor2 = tf.nn.relu(
			tf.nn.bias_add(tf.nn.conv2d(tensor, conv_5_w, strides=[1, 1, 1, 1], padding='SAME'), conv_5_b))

		for i in range(2):
			conv_w = tf.get_variable("conv_%02d_w" % (i + 6), [3, 3, f_size, f_size],
									 initializer=tf.random_normal_initializer(stddev=np.sqrt(2.0 / 9 / f_size)))
			conv_b = tf.get_variable("conv_%02d_b" % (i + 6), [f_size], initializer=tf.constant_initializer(0))
			weights.append(conv_w)
			weights.append(conv_b)
			tensor2 = tf.nn.relu(
				tf.nn.bias_add(tf.nn.conv2d(tensor2, conv_w, strides=[1, 1, 1, 1], padding='SAME'), conv_b))

		conv_w = tf.get_variable("conv_9_w", [3, 3, f_size, 1],
								 initializer=tf.random_normal_initializer(stddev=np.sqrt(2.0 / 9 / f_size)))
		conv_b = tf.get_variable("conv_9_b", [1], initializer=tf.constant_initializer(0))
		weights.append(conv_w)
		weights.append(conv_b)
		tensor2 = tf.nn.bias_add(tf.nn.conv2d(tensor2, conv_w, strides=[1, 1, 1, 1], padding='SAME'), conv_b)
		tensor2 = tf.add(tensor2, tensor)  # residual 2

		conv_10_w = tf.get_variable("conv_10_w", [3, 3, 1, f_size], initializer=tf.random_normal_initializer(stddev=np.sqrt(2.0 / 9)))
		conv_10_b = tf.get_variable("conv_10_b", [f_size], initializer=tf.constant_initializer(0))
		weights.append(conv_10_w)
		weights.append(conv_10_b)
		tensor3 = tf.nn.relu(
			tf.nn.bias_add(tf.nn.conv2d(tensor2, conv_10_w, strides=[1, 1, 1, 1], padding='SAME'), conv_10_b))

		for i in range(2):
			conv_w = tf.get_variable("conv_%02d_w" % (i + 11), [3, 3, f_size, f_size],
									 initializer=tf.random_normal_initializer(stddev=np.sqrt(2.0 / 9 / f_size)))
			conv_b = tf.get_variable("conv_%02d_b" % (i + 11), [f_size], initializer=tf.constant_initializer(0))
			weights.append(conv_w)
			weights.append(conv_b)
			tensor3 = tf.nn.relu(
				tf.nn.bias_add(tf.nn.conv2d(tensor3, conv_w, strides=[1, 1, 1, 1], padding='SAME'), conv_b))

		conv_w = tf.get_variable("conv_14_w", [3, 3, f_size, 1],
								 initializer=tf.random_normal_initializer(stddev=np.sqrt(2.0 / 9 / f_size)))
		conv_b = tf.get_variable("conv_14_b", [1], initializer=tf.constant_initializer(0))
		weights.append(conv_w)
		weights.append(conv_b)
		tensor3 = tf.nn.bias_add(tf.nn.conv2d(tensor3, conv_w, strides=[1, 1, 1, 1], padding='SAME'), conv_b)
		tensor3 = tf.add(tensor3, tensor2)  # residual3

		conv_15_w = tf.get_variable("conv_15_w", [3, 3, 1, f_size], initializer=tf.random_normal_initializer(stddev=np.sqrt(2.0 / 9)))
		conv_15_b = tf.get_variable("conv_15_b", [f_size], initializer=tf.constant_initializer(0))
		weights.append(conv_15_w)
		weights.append(conv_15_b)
		tensor4 = tf.nn.relu(
			tf.nn.bias_add(tf.nn.conv2d(tensor3, conv_15_w, strides=[1, 1, 1, 1], padding='SAME'), conv_15_b))

		for i in range(2):
			conv_w = tf.get_variable("conv_%02d_w" % (i + 16), [3, 3, f_size, f_size],
									 initializer=tf.random_normal_initializer(stddev=np.sqrt(2.0 / 9 / f_size)))
			conv_b = tf.get_variable("conv_%02d_b" % (i + 16), [f_size], initializer=tf.constant_initializer(0))
			weights.append(conv_w)
			weights.append(conv_b)
			tensor4 = tf.nn.relu(
				tf.nn.bias_add(tf.nn.conv2d(tensor4, conv_w, strides=[1, 1, 1, 1], padding='SAME'), conv_b))

		conv_w = tf.get_variable("conv_19_w", [3, 3, f_size, 1],
								 initializer=tf.random_normal_initializer(stddev=np.sqrt(2.0 / 9 / f_size)))
		conv_b = tf.get_variable("conv_19_b", [1], initializer=tf.constant_initializer(0))
		weights.append(conv_w)
		weights.append(conv_b)
		tensor4 = tf.nn.bias_add(tf.nn.conv2d(tensor4, conv_w, strides=[1, 1, 1, 1], padding='SAME'), conv_b)
		tensor = tf.add(tensor4, tensor3)  # residual 4
		return tensor, weights
