import numpy as np
import PIL
import os
import glob
import h5py
from TEST import rgb2ycbcr

train_list = []
data_path = "./data/291"    # YOU MUST CHECK THIS PATH
data_dir = os.path.join(os.getcwd(), data_path)
data = glob.glob(os.path.join(data_dir, "*.bmp"))
data += glob.glob(os.path.join(data_dir, "*.jpg"))
img = []
for z in range(4):
    img.append([])

for i in range(len(data)):
    img_pil = PIL.Image.open(data[i])
    img[0] = np.array(img_pil).astype(np.float32)
    img[0] = rgb2ycbcr(img[0])[:,:,0]/255.0

    width = img[0].shape[1]
    height = img[0].shape[0]
    img[0] = img[0][:height-divmod(height,12)[1], :width-divmod(width,12)[1]]
    patch_size = 41
    stride = 41
    x_size = int((img[0].shape[1]-patch_size)/stride+1)
    y_size = int((img[0].shape[0]-patch_size)/stride+1)

    for z in range(2,5):
        temp = img_pil.resize((int(img_pil.size[0] / z), int(img_pil.size[1] / z)), PIL.Image.BICUBIC)
        temp = temp.resize((int(img_pil.size[0]), int(img_pil.size[1])), PIL.Image.BICUBIC)
        img[z-1] = np.array(temp).astype(np.float32)
        img[z-1] = img[z-1][:height - divmod(height, 12)[1], :width - divmod(width, 12)[1]]
        img[z-1] = rgb2ycbcr(img[z-1])[:,:,0]/255.0

    for x in range(x_size):
        for y in range(y_size):
            x_coord = x*stride
            y_coord = y*stride

            patch = img[0][y_coord:y_coord + patch_size, x_coord:x_coord + patch_size]
            for z in range(1, 4):
                train_list.append([patch, img[z][y_coord:y_coord + patch_size, x_coord:x_coord + patch_size]])

            patch = np.rot90(img[0][y_coord:y_coord + patch_size, x_coord:x_coord + patch_size], 1)
            for z in range(1, 4):
                train_list.append([patch, np.rot90(img[z][y_coord:y_coord + patch_size, x_coord:x_coord + patch_size], 1)])

            patch = np.fliplr(img[0][y_coord:y_coord + patch_size, x_coord:x_coord + patch_size])
            for z in range(1, 4):
                train_list.append([patch, np.fliplr(img[z][y_coord:y_coord + patch_size, x_coord:x_coord + patch_size])])

            patch = np.fliplr(np.rot90(img[0][y_coord:y_coord + patch_size, x_coord:x_coord + patch_size], 1))
            for z in range(1, 4):
                train_list.append([patch, np.fliplr(np.rot90(img[z][y_coord:y_coord + patch_size, x_coord:x_coord + patch_size], 1))])
    print(i)    # up to 291 ( 0~291 )
with h5py.File('./data/train.h5','w') as hf:
    hf.create_dataset("train", data=train_list)
print("Success process")